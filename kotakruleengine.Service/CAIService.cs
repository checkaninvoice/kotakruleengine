﻿using kotakruleengine.Service.TransportModel;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace kotakruleengine.Service
{
    public class CAIService
    {
        private readonly HttpClient _httpClient;
        public CAIService(HttpClient clientFactory)
        {
            _httpClient = clientFactory;

        }
        public async Task<string> AddToEmailNotification(EmailNotification emailNotification)
        {
            try
            {
                var content = new StringContent(JsonSerializer.Serialize(emailNotification), Encoding.UTF8, "application/json");
                using HttpResponseMessage httpResponseMessage = await this._httpClient.PostAsync($"MailAccount/AddToEmailNotification", content);
                return "";
            }
            catch(Exception e)
            {
                throw;
            }

        }

    }
}
