﻿using System;
using System.Collections.Generic;
using System.Text;

namespace kotakruleengine.Service.TransportModel
{
    public class EmailNotification
    {
        public long Id { get; set; }
        public string To { get; set; }
        public string CC { get; set; }
        public string Subject { get; set; }
        public string DisplayName { get; set; }
        public string Body { get; set; }
        public DateTime EmailCreatedOn { get; set; }
        public DateTime? EmailSentOn { get; set; }
        public string Source { get; set; }
        public int Status { get; set; }
        public int NotificationTypeId { get; set; }
    }
}
