﻿using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace kotakruleengine.Service.Logger
{
    class LoggerService: ILoggerService
    {
        private IHostEnvironment _hostEnvironment;

        public LoggerService(IHostEnvironment hostEnvironment)
        {
            _hostEnvironment = hostEnvironment;
        }
        public void LogInfo(string info)
        {
            string contentRootPath = _hostEnvironment.ContentRootPath;

            string DirectoryPath = Path.Combine(_hostEnvironment.ContentRootPath, "LogInfo");
            Directory.CreateDirectory(DirectoryPath);

            string FileName = DateTime.UtcNow.ToString("MM-dd-yyyy") + ".txt";
            string FilePath = Path.Combine(DirectoryPath, FileName);

            using (StreamWriter streamWriter = new StreamWriter(FilePath, true))
            {
                streamWriter.WriteLine(DateTime.Now);
                streamWriter.WriteLine(info);
                streamWriter.WriteLine("____________________________________________________________________________________________");
                streamWriter.WriteLine();
            }
        }
    }
}
