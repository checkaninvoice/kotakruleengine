﻿using System;
using System.Collections.Generic;
using System.Text;

namespace kotakruleengine.Service.Logger
{
    public interface ILoggerService
    {
        void LogInfo(string data);
    }
}
