﻿using kotakruleengine.Data.Model;
using kotakruleengine.Service.TransportModel;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace kotakruleengine.Service
{
    public class Email
    {
        private readonly HttpClient _httpClient;
        IConfiguration _config;
        public Email(HttpClient clientFactory, IConfiguration config)
        {
            _httpClient = clientFactory;
            _config = config;
        }
        public async Task<string> SendExceptionMail(List<NotificationPreference> notiPreference, string UploadedBy, int panExceptionCount, int MissingFieldExceptionCount, string firstAccountHolderName, string MissingFieldDetails, string PanExceptionMsg) //Fraud Exception mail shooting
        {
            try
            {
                EmailNotification notificationsObj = new EmailNotification();
                notificationsObj.Subject = "Exceptions Alert";
                notificationsObj.To = UploadedBy;
                notificationsObj.CC = string.Empty;
                notificationsObj.Source = "KotakRuleEngine";
                int Type = 0;
                bool IsExceptionNoti = false;
                bool IsMissingFieldNoti = false;
                IsMissingFieldNoti = notiPreference.Where(x => x.NotificationTypeId == 1 && x.IsActive == true).FirstOrDefault() != null ? true : false;
                IsExceptionNoti = notiPreference.Where(x => x.NotificationTypeId == 2 && x.IsActive == true).FirstOrDefault() != null ? true : false;
                if (panExceptionCount > 0 && IsExceptionNoti && MissingFieldExceptionCount > 0 && IsMissingFieldNoti)
                {
                    Type = 1;
                }
                else if (MissingFieldExceptionCount > 0 && IsMissingFieldNoti)
                {
                    Type = 2;
                }
                else if (panExceptionCount > 0 && IsExceptionNoti)
                {
                    Type = 3;
                }

                if (Type != 0)
                {
                    notificationsObj.Body = this.CreateEmailExceptionBody(notiPreference, Type, firstAccountHolderName, MissingFieldDetails, PanExceptionMsg);
                    CAIService cAIService = new CAIService(_httpClient);
                    await cAIService.AddToEmailNotification(notificationsObj);
                }
                return "";
            }
            catch (Exception e)
            {
                throw;
            }
        }

        private string CreateEmailExceptionBody(List<NotificationPreference> notiPreference, int type, string NameFirstAccountHolder, string missingfields, string PanExceptionMsg)
        {
            string body = string.Empty;
            try
            {
                var path = AssemblyDirectory + _config.GetValue<string>("AppSettings:ExceptionTemplate1");
                body = File.ReadAllText(Path.Combine(path));
                if (type == 1)
                {
                    body = body.Replace("{ExceptionDetails}", notiPreference.Where(x => x.NotificationTypeId == 2).FirstOrDefault().Template);
                    body = body.Replace("{PANException}", PanExceptionMsg);
                    body = body.Replace("{MissingFieldDetails}", notiPreference.Where(x => x.NotificationTypeId == 1).FirstOrDefault().Template);
                    body = body.Replace("{Name of First Account Holder}", NameFirstAccountHolder);
                    body = body.Replace("{MissingField}", missingfields);
                }
                else if (type == 2)
                {
                    body = body.Replace("{MissingFieldDetails}", notiPreference.Where(x => x.NotificationTypeId == 1).FirstOrDefault().Template);
                    body = body.Replace("{Name of First Account Holder}", NameFirstAccountHolder);
                    body = body.Replace("{MissingField}", missingfields);
                    body = body.Replace("{ExceptionDetails}", "");
                }
                else if (type == 3)
                {
                    body = body.Replace("{ExceptionDetails}", notiPreference.Where(x => x.NotificationTypeId == 2).FirstOrDefault().Template);
                    body = body.Replace("{PANException}", PanExceptionMsg);
                    body = body.Replace("{Name of First Account Holder}", NameFirstAccountHolder);
                    body = body.Replace("{MissingFieldDetails}", "");
                }
                return body;
            }
            catch (Exception e)
            {
                throw;
            }
        }
        private string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }
    }

}
