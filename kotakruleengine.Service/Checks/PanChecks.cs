﻿using kotakruleengine.Data;
using kotakruleengine.Data.Model;
using kotakruleengine.Service.Logger;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace kotakruleengine.Service.Checks
{
    public class PanChecks : Check
    {
        DataLayer obj;
        UnProcessedDocuments _document;
        private readonly ILoggerService _loggerService;
        private readonly IConfiguration _config;

        public PanChecks(ApplicationDbContext ConStr, UnProcessedDocuments doc, ILoggerService loggerService, IConfiguration config) : base(ConStr, loggerService, config)
        {
            _config = config;
            obj = new DataLayer(ConStr, _config);
            _document = doc;
            _loggerService = loggerService;


        }

        public override string ExcecuteRules(List<CompanyRules> companyRules, List<CheckField> checkFields, out DataTable CheckResultsDataTable, out int ExceptionCount)
        {
            ExceptionCount = 0;
            CheckResultsDataTable = new DataTable();
            CheckResultsDataTable.Columns.AddRange(new DataColumn[5] { new DataColumn("DocumentId", typeof(long)),
                    new DataColumn("CheckId", typeof(int)),
                    new DataColumn("IsPassed",typeof(bool)), new DataColumn("Details",typeof(string))
            , new DataColumn("DateTime",typeof(DateTime))});
            string PanExceptionMsg = string.Empty;

            if (companyRules != null && companyRules.Count > 0)
            {
                for (int i = 0; i < companyRules.Count; i++)
                {
                    if (companyRules[i].CheckName == "Missing Pan Check" || companyRules[i].CheckName == "PAN Name Check")
                    {
                        List<KotakPans> kotakPansDetail = obj.GetKotakPans(_document.KotakApplicationFieldID);

                        int TotalAccountName = 0;
                        List<string> panSubmittedstring = new List<string>();
                        List<string> panNamestring = new List<string>();
                        bool pancheck = false;
                        if (!string.IsNullOrEmpty(_document.namefirstacchol))
                        {
                            TotalAccountName++;
                            KotakPans KPfirstacchol = kotakPansDetail.Where(d => d.Name.ToLower() == _document.namefirstacchol.ToLower() && d.KotakApplicationFileId == _document.KotakApplicationFieldID).FirstOrDefault();
                            if (KPfirstacchol == null)
                            {
                                pancheck = true;
                                panSubmittedstring.Add("No PAN submitted for " + _document.namefirstacchol);
                                //panNamestring.Add("Application form name of user " + _document.namefirstacchol + " does not match with the Pan name.");
                                panNamestring.Add("Submitted PAN does not match with the account holder's name.");
                            }
                        }
                        if (!string.IsNullOrEmpty(_document.firstname))
                        {
                            TotalAccountName++;
                            KotakPans KPfirstacchol = kotakPansDetail.Where(d => d.Name.ToLower() == _document.firstname.ToLower() && d.KotakApplicationFileId == _document.KotakApplicationFieldID).FirstOrDefault();
                            if (KPfirstacchol == null)
                            {
                                pancheck = true;
                                panSubmittedstring.Add("No PAN submitted for " + _document.firstname);
                                //panNamestring.Add("Application form name of user " + _document.firstname + " does not match with the Pan name.");
                                if (panNamestring.Count == 0)
                                    panNamestring.Add("Submitted PAN does not match with the account holder's name.");
                            }
                        }
                        if (!string.IsNullOrEmpty(_document.secondname))
                        {
                            TotalAccountName++;
                            KotakPans KPfirstacchol = kotakPansDetail.Where(d => d.Name.ToLower() == _document.secondname.ToLower() && d.KotakApplicationFileId == _document.KotakApplicationFieldID).FirstOrDefault();
                            if (KPfirstacchol == null)
                            {
                                pancheck = true;
                                panSubmittedstring.Add("No PAN submitted for " + _document.secondname);
                                //panNamestring.Add("Application form name of user " + _document.secondname + " does not match with the Pan name.");
                                if (panNamestring.Count == 0)
                                    panNamestring.Add("Submitted PAN does not match with the account holder's name.");
                            }
                        }
                        if (!string.IsNullOrEmpty(_document.thirdname))
                        {
                            TotalAccountName++;
                            KotakPans KPfirstacchol = kotakPansDetail.Where(d => d.Name.ToLower() == _document.thirdname.ToLower() && d.KotakApplicationFileId == _document.KotakApplicationFieldID).FirstOrDefault();
                            if (KPfirstacchol == null)
                            {
                                pancheck = true;
                                panSubmittedstring.Add("No PAN submitted for " + _document.thirdname);
                                //panNamestring.Add("Application form name of user " + _document.thirdname + " does not match with the Pan name.");
                                if (panNamestring.Count == 0)
                                    panNamestring.Add("Submitted PAN does not match with the account holder's name.");
                            }
                        }

                        //Missing Pan Check
                        if (companyRules[i].CheckName == "Missing Pan Check")
                        {
                            if (TotalAccountName > kotakPansDetail.Count && pancheck == true)
                            {

                                string details = string.Empty;
                                foreach (string s in panSubmittedstring)
                                {
                                    details += s + ", ";
                                }
                                details = details.TrimEnd(',').Trim().TrimEnd(',');
                                PanExceptionMsg = details;
                                CheckResultsDataTable.Rows.Add(_document.Id, 4, false, details, DateTime.UtcNow);
                                ExceptionCount++;
                            }
                            else
                                CheckResultsDataTable.Rows.Add(_document.Id, 4, true, "", DateTime.UtcNow);
                        }
                        if (companyRules[i].CheckName == "PAN Name Check")
                        {
                            if (TotalAccountName <= kotakPansDetail.Count)
                            {
                                //Pan Name Check
                                if (pancheck)
                                {
                                    string detail = string.Empty;
                                    foreach (string s in panNamestring)
                                    {
                                        detail += s + ", ";
                                    }
                                    detail = detail.TrimEnd(',').Trim().TrimEnd(',');
                                    PanExceptionMsg = detail;
                                    CheckResultsDataTable.Rows.Add(_document.Id, 2, false, detail, DateTime.UtcNow);
                                    ExceptionCount++;
                                }
                                else
                                    CheckResultsDataTable.Rows.Add(_document.Id, 2, true, "", DateTime.UtcNow);
                            }
                        }
                    }
                }
            }
            return PanExceptionMsg;
        }
    }
}
