﻿using kotakruleengine.Data;
using kotakruleengine.Data.Model;
using kotakruleengine.Service.Logger;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace kotakruleengine.Service.Checks
{
    public class MissingFieldsCheck : Check
    {
        DataLayer obj;
        UnProcessedDocuments _document;
        private readonly ILoggerService _loggerService;
        private readonly IConfiguration _config;
        public MissingFieldsCheck(ApplicationDbContext ConStr, UnProcessedDocuments doc, ILoggerService loggerService, IConfiguration config) : base(ConStr, loggerService, config)
        {
            _config = config;
            obj = new DataLayer(ConStr, _config);
            _document = doc;
            _loggerService = loggerService;


        }
        public override string ExcecuteRules(List<CompanyRules> companyRules, List<CheckField> checkFields, out DataTable CheckResultsDataTable, out int ExceptionCount)
        {
            ExceptionCount = 0;
            CheckResultsDataTable = new DataTable();
            CheckResultsDataTable.Columns.AddRange(new DataColumn[5] { new DataColumn("DocumentId", typeof(long)),
                    new DataColumn("CheckId", typeof(int)),
                    new DataColumn("IsPassed",typeof(bool)), new DataColumn("Details",typeof(string))
            , new DataColumn("DateTime",typeof(DateTime))});
            string details = string.Empty;
            if (companyRules != null && companyRules.Count > 0)
            {
                for (int i = 0; i < companyRules.Count; i++)
                {
                    if (companyRules[i].CheckName == "Missing Value Check")
                    {
                        KotakApplicationFile kotakApplicationFile = obj.GetKotakApplicationFile(_document.KotakApplicationFieldID);

                        List<string> emptyFields = new List<string>();

                        if (string.IsNullOrEmpty(kotakApplicationFile.PleaseOpenmyAcc) && checkFields.Where(x => x.FieldName == "Please Open My Account").Count() > 0)
                            emptyFields.Add("Please Open my account");
                        if (string.IsNullOrEmpty(kotakApplicationFile.ModeofOprtn) && checkFields.Where(x => x.FieldName == "Mode of Operation").Count() > 0)
                            emptyFields.Add("Mode of Operation");
                        if (string.IsNullOrEmpty(kotakApplicationFile.SavngVariant) && checkFields.Where(x => x.FieldName == "Savings Variant").Count() > 0)
                            emptyFields.Add("Savings Variant");
                        if (string.IsNullOrEmpty(kotakApplicationFile.MinAvgMonthlyBal) && checkFields.Where(x => x.FieldName == "Minimum Average Monthly Balance").Count() > 0)
                            emptyFields.Add("Minimum Average Monthly Balance");
                        if (string.IsNullOrEmpty(kotakApplicationFile.PrivyLeague) && checkFields.Where(x => x.FieldName == "Privy League").Count() > 0)
                            emptyFields.Add("Privy League");
                        if (string.IsNullOrEmpty(kotakApplicationFile.NameFirstAccHol) && checkFields.Where(x => x.FieldName == "Name of First Account Holder").Count() > 0)
                            emptyFields.Add("Name of First Account Holder");

                        if (string.IsNullOrEmpty(kotakApplicationFile.NameAddnlJtAccHol) && checkFields.Where(x => x.FieldName == "Names of Additional Joint Account Holders").Count() > 0)
                            emptyFields.Add("Names of Additional Joint Account Holders");
                        if (string.IsNullOrEmpty(kotakApplicationFile.FirstName) && checkFields.Where(x => x.FieldName == "1st Name").Count() > 0)
                            emptyFields.Add("1st Name");
                        if (string.IsNullOrEmpty(kotakApplicationFile.SecondName) && checkFields.Where(x => x.FieldName == "2nd Name").Count() > 0)
                            emptyFields.Add("2nd Name");
                        if (string.IsNullOrEmpty(kotakApplicationFile.ThirdName) && checkFields.Where(x => x.FieldName == "3rd Name").Count() > 0)
                            emptyFields.Add("3rd Name");

                        if (string.IsNullOrEmpty(kotakApplicationFile.InitialPayDet) && checkFields.Where(x => x.FieldName == "Initial Payment Details").Count() > 0)
                            emptyFields.Add("Initial Payment Details");
                        if (string.IsNullOrEmpty(kotakApplicationFile.KMBLAccNoorChqno) && checkFields.Where(x => x.FieldName == "KMBL Account Number or Cheque No").Count() > 0)
                            emptyFields.Add("KMBL Account Number or Cheque no");
                        if (string.IsNullOrEmpty(kotakApplicationFile.ChequeDate) && checkFields.Where(x => x.FieldName == "Cheque Date").Count() > 0)
                            emptyFields.Add("Cheque date");
                        if (string.IsNullOrEmpty(kotakApplicationFile.BankName) && checkFields.Where(x => x.FieldName == "Bank Name").Count() > 0)
                            emptyFields.Add("Bank name");
                        if (string.IsNullOrEmpty(kotakApplicationFile.Branch) && checkFields.Where(x => x.FieldName == "Branch").Count() > 0)
                            emptyFields.Add("Branch");
                        if (string.IsNullOrEmpty(kotakApplicationFile.IFSCCode) && checkFields.Where(x => x.FieldName == "IFSC Code").Count() > 0)
                            emptyFields.Add("IFSC code");
                        if (string.IsNullOrEmpty(kotakApplicationFile.CustSign) && checkFields.Where(x => x.FieldName == "Customer Signature").Count() > 0)
                            emptyFields.Add("Customer Signature");
                        if (string.IsNullOrEmpty(kotakApplicationFile.AccStatemntPassbk) && checkFields.Where(x => x.FieldName == "Account Statement or Passbook").Count() > 0)
                            emptyFields.Add("Account Statement or Passbook");
                        if (string.IsNullOrEmpty(kotakApplicationFile.EmpName) && checkFields.Where(x => x.FieldName == "Employee Name").Count() > 0)
                            emptyFields.Add("Employee Name");
                        if (string.IsNullOrEmpty(kotakApplicationFile.EmpCodeNo) && checkFields.Where(x => x.FieldName == "Employee Code Number").Count() > 0)
                            emptyFields.Add("Employee Code Number");
                        if (string.IsNullOrEmpty(kotakApplicationFile.DateofJoining) && checkFields.Where(x => x.FieldName == "Date of Joining").Count() > 0)
                            emptyFields.Add("Date of Joining");
                        if (string.IsNullOrEmpty(kotakApplicationFile.CreditFacilities) && checkFields.Where(x => x.FieldName == "Credit Facilities").Count() > 0)
                            emptyFields.Add("Credit Facilities");
                        if (string.IsNullOrEmpty(kotakApplicationFile.DepositPeriod) && checkFields.Where(x => x.FieldName == "Period").Count() > 0)
                            emptyFields.Add("Deposit Period");
                        if (string.IsNullOrEmpty(kotakApplicationFile.DepositMonth) && checkFields.Where(x => x.FieldName == "Month").Count() > 0)
                            emptyFields.Add("Deposit Month");
                        if (string.IsNullOrEmpty(kotakApplicationFile.DepositDay) && checkFields.Where(x => x.FieldName == "Day").Count() > 0)
                            emptyFields.Add("Deposit Day");
                        if (string.IsNullOrEmpty(kotakApplicationFile.DepositAmount) && checkFields.Where(x => x.FieldName == "Amount").Count() > 0)
                            emptyFields.Add("Deposit Amount");
                        if (string.IsNullOrEmpty(kotakApplicationFile.IntrstFrequency) && checkFields.Where(x => x.FieldName == "Interest Frequency").Count() > 0)
                            emptyFields.Add("Interest Frequency");
                        if (string.IsNullOrEmpty(kotakApplicationFile.ReInvestment) && checkFields.Where(x => x.FieldName == "Reinvestment").Count() > 0)
                            emptyFields.Add("Reinvestment");
                        if (string.IsNullOrEmpty(kotakApplicationFile.PayoutQuarterly) && checkFields.Where(x => x.FieldName == "Payout Quarterly").Count() > 0)
                            emptyFields.Add("Payout Quarterly");
                        if (string.IsNullOrEmpty(kotakApplicationFile.PayoutMonthly) && checkFields.Where(x => x.FieldName == "Payout Monthly").Count() > 0)
                            emptyFields.Add("Payout Monthly");
                        if (string.IsNullOrEmpty(kotakApplicationFile.IntrstPayMatPaytIns) && checkFields.Where(x => x.FieldName == "Interest Payment And Maturity Payment Instructions").Count() > 0)
                            emptyFields.Add("Interest Payment And Maturity Payment Instructions");
                        if (string.IsNullOrEmpty(kotakApplicationFile.SweepInFacilityReq) && checkFields.Where(x => x.FieldName == "Sweep In Facility Required").Count() > 0)
                            emptyFields.Add("Sweep In Facility Required");
                        if (string.IsNullOrEmpty(kotakApplicationFile.ActivMoneyFacilityReq) && checkFields.Where(x => x.FieldName == "Active Money Facility Required").Count() > 0)
                            emptyFields.Add("Activ Money Facility Required");
                        if (string.IsNullOrEmpty(kotakApplicationFile.IWeSubscribedProduct) && checkFields.Where(x => x.FieldName == "I or We have Subscribed to the Product").Count() > 0)
                            emptyFields.Add("I or We have Subscribed to the Product");
                        if (string.IsNullOrEmpty(kotakApplicationFile.IWeUnderstandThatOpenedAccUnder) && checkFields.Where(x => x.FieldName == "I or We Understand and Agree that I or We have opened account under").Count() > 0)
                            emptyFields.Add("I or We understand and agree that I or We have opened account under");
                        if (string.IsNullOrEmpty(kotakApplicationFile.FirstHolder) && checkFields.Where(x => x.FieldName == "1st holder").Count() > 0)
                            emptyFields.Add("1st holder");

                        if (string.IsNullOrEmpty(kotakApplicationFile.SecondHolder) && checkFields.Where(x => x.FieldName == "2nd holder").Count() > 0)
                            emptyFields.Add("2nd holder");
                        if (string.IsNullOrEmpty(kotakApplicationFile.ThirdHolder) && checkFields.Where(x => x.FieldName == "3rd holder").Count() > 0)
                            emptyFields.Add("3rd holder");
                        if (string.IsNullOrEmpty(kotakApplicationFile.FourthHolder) && checkFields.Where(x => x.FieldName == "4th holder").Count() > 0)
                            emptyFields.Add("4th holder");

                        if (emptyFields.Count > 0)
                        {
                            string missingField = "";
                            details = "<ul>";
                            foreach (string s in emptyFields)
                            {
                                details += "<li>" + s + "</li>";
                                missingField = missingField + s + ",";
                            }
                            details += "</ul>";
                            missingField = missingField.TrimEnd(',');
                            missingField = missingField + " Fields missing.";


                            CheckResultsDataTable.Rows.Add(_document.Id, 1, false, missingField, DateTime.UtcNow);
                            ExceptionCount++;
                        }
                        else
                            CheckResultsDataTable.Rows.Add(_document.Id, 1, true, "", DateTime.UtcNow);
                    }
                }
            }
            return details;
        }

    }

}
