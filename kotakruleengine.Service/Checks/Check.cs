﻿using kotakruleengine.Data;
using kotakruleengine.Data.Model;
using kotakruleengine.Service.Logger;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace kotakruleengine.Service.Checks
{
    public abstract class Check
    {
        private readonly ApplicationDbContext _context;
        public string Remark { get; set; }
        private readonly ILoggerService _loggerService;
        private readonly IConfiguration _config;

        public Check(ApplicationDbContext context, ILoggerService loggerService, IConfiguration config)
        {
            _context = context;
            _loggerService = loggerService;
            _config = config;

        }
        public abstract string ExcecuteRules(List<CompanyRules> companyRules, List<CheckField> checkFields, out DataTable dtCheckResults, out int ECount);

        public void UpdateCheckResult(DataTable dtCheckResults)
        {
            try
            {
                DataLayer obj = new DataLayer(_context, _config);
                obj.InsertBulkData(dtCheckResults, "CheckResults");
            }
            catch (Exception e)
            {
                _loggerService.LogInfo("UpdateCheckResult:" + e.ToString());
            }
        }

        public void UpdateResults(long DocumentId,int ExceptionCount)
        {
            try
            {
                string Status = ExceptionCount > 0 ? "Exception" : "No Exception";
                DataLayer obj = new DataLayer(_context, _config);
                obj.UpdateResult(DocumentId, Status);
            }
            catch (Exception e)
            {
                _loggerService.LogInfo("UpdateResults with docid:"+ DocumentId +"Exception "+ e.ToString());

            }
        }

    }
}
