using kotakruleengine.Data;
using kotakruleengine.Data.Model;
using kotakruleengine.Service.Checks;
using kotakruleengine.Service.Logger;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace kotakruleengine.Service
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private readonly ILoggerService _loggerService;
        DataLayer _dataLayerobj;
        private readonly ApplicationDbContext _context;
        private readonly IConfiguration _config;
        private readonly HttpClient _httpClient;

        public Worker(ILogger<Worker> logger, ILoggerService loggerService, ApplicationDbContext context, IConfiguration config, IHttpClientFactory clientFactory)
        {
            _logger = logger;
            _loggerService = loggerService;
            _context = context;
            _config = config;
            _dataLayerobj = new DataLayer(_context, _config);
            _httpClient = clientFactory.CreateClient("CAI-WebAPI");
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
                await Task.Delay(1000, stoppingToken);

                try
                {
                    DataSet DataSet = await _dataLayerobj.GetUnProcessedDocumentsAndCompanyRules();
                    IList<UnProcessedDocuments> unProcessedList = (from rw in DataSet.Tables[0].AsEnumerable()
                                                                   select new UnProcessedDocuments()
                                                                   {
                                                                       Id = Convert.ToInt32(rw["ID"]),
                                                                       Email = Convert.ToString(rw["Email"]),
                                                                       XtractaDocId = Convert.ToString(rw["XtractaDocId"]),
                                                                       UploadedBy = Convert.ToString(rw["UploadedBy"]),
                                                                       KotakApplicationFieldID = Convert.ToInt32(rw["KotakApplicationFieldID"]),
                                                                       namefirstacchol = Convert.ToString(rw["namefirstacchol"]),
                                                                       firstname = Convert.ToString(rw["firstname"]),
                                                                       secondname = Convert.ToString(rw["secondname"]),
                                                                       thirdname = Convert.ToString(rw["thirdname"])

                                                                   }).ToList();
                    List<CompanyRules> companyRules = DataSet.Tables[1].AsEnumerable().Select(row =>
                                          new CompanyRules
                                          {
                                              RuleName = row.Field<string>("RuleName"),
                                              CheckName = row.Field<string>("CheckName")
                                          }).ToList();

                    List<NotificationPreference> notiPreference = DataSet.Tables[2].AsEnumerable().Select(row =>
                                          new NotificationPreference
                                          {
                                              NotificationTypeId = row.Field<long>("NotificationTypeId"),
                                              Description = row.Field<string>("Description"),
                                              Template = row.Field<string>("Template"),
                                              IsActive = row.Field<bool>("IsActive")
                                          }).ToList();

                    List<CheckField> checkFields = DataSet.Tables[3].AsEnumerable().Select(row =>
                                          new CheckField
                                          {
                                              FieldName = row.Field<string>("FieldName")
                                          }).ToList();

                    if (unProcessedList != null && unProcessedList.Count > 0)
                    {
                        foreach (UnProcessedDocuments document in unProcessedList)
                        {
                            // Execute Rule
                            await ExecuteRules(document, companyRules, notiPreference, checkFields);
                        }
                    }
                }
                catch (Exception e)
                {
                    _loggerService.LogInfo("ExecuteAsync:" + e.ToString());
                }
            }
        }
        private async Task<string> ExecuteRules(UnProcessedDocuments document, List<CompanyRules> companyRules, List<NotificationPreference> notiPreference, List<CheckField> checkFields)
        {
            int PanExceptionCount = 0;
            int MissingFieldExceptionCount = 0;
            int TotalExceptionCount = 0;
            string missingField = "";
            string PanExceptionMsg = "";
            try
            {

                DataTable PanCheckResultsDatatable = new DataTable();
                Check pancheck = new PanChecks(_context, document, _loggerService, _config);
                PanExceptionMsg = pancheck.ExcecuteRules(companyRules, checkFields, out PanCheckResultsDatatable, out PanExceptionCount);

                DataTable MissingFieldCheckResultsDatatable = new DataTable();
                Check Missingfieldcheck = new MissingFieldsCheck(_context, document, _loggerService, _config);
                missingField = Missingfieldcheck.ExcecuteRules(companyRules, checkFields, out MissingFieldCheckResultsDatatable, out MissingFieldExceptionCount);

                //Merge the CheckResults 
                PanCheckResultsDatatable.Merge(MissingFieldCheckResultsDatatable);
                TotalExceptionCount = PanExceptionCount + MissingFieldExceptionCount;

                //Insert in CheckResult
                pancheck.UpdateCheckResult(PanCheckResultsDatatable);

                //Insert in Results
                pancheck.UpdateResults(document.Id, TotalExceptionCount);
            }
            catch (Exception e)
            {
                _loggerService.LogInfo("DocumentId " + document.Id + ",Exception:" + e.ToString());
            }
            finally
            {
                if (TotalExceptionCount > 0)
                    await SaveEmailNotification(notiPreference, document.Id, document.Email, PanExceptionCount, MissingFieldExceptionCount, document.namefirstacchol, missingField, PanExceptionMsg);
            }

            return "";
        }

        private async Task<string> SaveEmailNotification(List<NotificationPreference> notiPreference, long documentId, string UploadedBy, int panExceptionCount, int MissingFieldExceptionCount, string firstAccountHolderName, string MissingFieldDetails, string PanExceptionMsg)
        {
            try
            {
                Email email = new Email(_httpClient, _config);
                await email.SendExceptionMail(notiPreference, UploadedBy, panExceptionCount, MissingFieldExceptionCount, firstAccountHolderName, MissingFieldDetails, PanExceptionMsg);
            }
            catch (Exception e)
            {
                _loggerService.LogInfo("Method: SaveEmailNotification ,DocumentId" + documentId + ",Exception:" + e.ToString());
            }
            return "";
        }
    }
}