using kotakruleengine.Data;
using kotakruleengine.Service.Logger;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace kotakruleengine.Service
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
            .UseWindowsService()
                .ConfigureServices((hostContext, services) =>
                {
                    IConfiguration _config = hostContext.Configuration;
                    services.AddHostedService<Worker>();

                    string DB = _config.GetValue<string>("ConnectionStrings:DataBaseString");
                    services.AddDbContext<ApplicationDbContext>(
                        option => option.UseSqlServer(DB),
                        ServiceLifetime.Singleton,
                        ServiceLifetime.Singleton
                        );

                    services.AddSingleton<ILoggerService, LoggerService>();
                    services.AddHttpClient(
                    "CAI-WebAPI", c =>
                    {
                        c.DefaultRequestHeaders
                        .Accept
                        .Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                        c.DefaultRequestHeaders
                        .TryAddWithoutValidation("AppName", "KotakRuleEngine");
                        c.BaseAddress = new Uri(_config.GetValue<string>("CheckAnInvoice:WebAPi-Url"));
                    });
                });
    }
}
