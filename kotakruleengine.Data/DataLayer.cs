﻿using kotakruleengine.Data.Model;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace kotakruleengine.Data
{
    public class DataLayer
    {
        private readonly ApplicationDbContext _context;
        private readonly IConfiguration _config;
        public DataLayer(ApplicationDbContext context, IConfiguration config)
        {
            _context = context;
            _config = config;
        }
        public async Task<DataSet> GetUnProcessedDocumentsAndCompanyRules()
        {
            var ds = new DataSet();
            try
            {
                using (var command = _context.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = "GetUnScannedDocuments";
                    command.CommandType = CommandType.StoredProcedure;
                    _context.Database.OpenConnection();
                    using (SqlDataAdapter adapter = new SqlDataAdapter((SqlCommand)command))
                    {
                       
                        adapter.Fill(ds);
                        return ds;
                    }
                }

            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public List<KotakPans> GetKotakPans(long KotakApplicationId)
        {
            try
            {
                var ApplicationFieldID = new SqlParameter("@ApplicationFieldID", KotakApplicationId);
               

                List<KotakPans> DataList = _context.Set<KotakPans>().FromSqlRaw(
                    $"EXEC [dbo].[GetApplicationKotakPans] @ApplicationFieldID", ApplicationFieldID
                ).ToList();

                return DataList;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public KotakApplicationFile GetKotakApplicationFile(long Id)
        {
            try
            {
                var KotakApplicationFilesId = new SqlParameter("@KotakApplicationFilesID", Id);


                List<KotakApplicationFile> Data = _context.Set<KotakApplicationFile>().FromSqlRaw(
                    $"EXEC [dbo].[GetKotakApplicationFile] @KotakApplicationFilesID", KotakApplicationFilesId
                ).ToList();
                if(Data!=null)
                    return Data[0];
                return null;
     
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void InsertBulkData(DataTable table, string TableName)
        {

            string sqlcon = _config.GetValue<string>("ConnectionStrings:DataBaseString");
            using (var bulkCopy = new SqlBulkCopy(sqlcon, SqlBulkCopyOptions.KeepIdentity))
            {
                // my DataTable column names match my SQL Column names, so I simply made this loop. However if your column names don't match, just pass in which datatable name matches the SQL column name in Column Mappings
                foreach (DataColumn col in table.Columns)
                {
                    bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                }

                bulkCopy.BulkCopyTimeout = 600;
                bulkCopy.DestinationTableName = TableName;
                bulkCopy.WriteToServer(table);
            }

        }

        public void UpdateResult(long id, string Status)
        {
            try
            {
              
                var commandText = "EXEC InsertDocumentResult @DocumentId,@Status";
                var Id = new SqlParameter("@DocumentId", id);
                var Stus = new SqlParameter("@Status", Status);
                _context.Database.ExecuteSqlCommand(commandText, Id, Stus);

            }
            catch (Exception e)
            {
                throw e;
            }

        }

    }

}
