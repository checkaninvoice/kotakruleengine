﻿using System;
using System.Collections.Generic;
using System.Text;

namespace kotakruleengine.Data.Model
{
    public class UnProcessedDocuments
    {
        public long Id { get; set; }
        public string Email { get; set; }
        public string XtractaDocId { get; set; }
        public string UploadedBy { get; set; }
        public long KotakApplicationFieldID { get; set; }
        public string namefirstacchol { get; set; }
        public string firstname { get; set; }
        public string secondname { get; set; }
        public string thirdname { get; set; }
    }
}
