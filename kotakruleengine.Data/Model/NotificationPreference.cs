﻿using System;
using System.Collections.Generic;
using System.Text;

namespace kotakruleengine.Data.Model
{
    public class NotificationPreference
    {
        public long NotificationTypeId { get; set; }
        public string Description { get; set; }
        public string Template { get; set; }
        public bool IsActive { get; set; }
    }
}
