﻿using System;
using System.Collections.Generic;
using System.Text;

namespace kotakruleengine.Data.Model
{
    public class KotakApplicationFile
    {

        public long Id { get; set; }
       
        public string XtractaDocId { get; set; }
       
        public string PleaseOpenmyAcc { get; set; }
      
        public string ModeofOprtn { get; set; }

        public string SavngVariant { get; set; }
   
        public string MinAvgMonthlyBal { get; set; }
    
        public string PrivyLeague { get; set; }
      
        public string NameFirstAccHol { get; set; }
       
        public string NameAddnlJtAccHol { get; set; }
       
        public string FirstName { get; set; }
 
        public string SecondName { get; set; }
        public string ThirdName { get; set; }
 
        public string InitialPayDet { get; set; }

        public string KMBLAccNoorChqno { get; set; }

        public string ChequeDate { get; set; }

        public string BankName { get; set; }

        public string Branch { get; set; }

        public string IFSCCode { get; set; }

        public string CustSign { get; set; }

  
        public string AccStatemntPassbk { get; set; }

        public string EmpName { get; set; }

        public string EmpCodeNo { get; set; }

        public string DateofJoining { get; set; }
   
        public string CreditFacilities { get; set; }
        
        public string DepositPeriod { get; set; }
        
        public string DepositMonth { get; set; }
       
        public string DepositDay { get; set; }
      
        public string DepositAmount { get; set; }
        
        public string IntrstFrequency { get; set; }
       
        public string ReInvestment { get; set; }
       
        public string PayoutQuarterly { get; set; }
      
        public string PayoutMonthly { get; set; }
        
        public string IntrstPayMatPaytIns { get; set; }
       
        public string SweepInFacilityReq { get; set; }
 
        public string ActivMoneyFacilityReq { get; set; }

        public string IWeSubscribedProduct { get; set; }

        public string IWeUnderstandThatOpenedAccUnder { get; set; }

        public string FirstHolder { get; set; }
     
        public string SecondHolder { get; set; }
     
        public string ThirdHolder { get; set; }
      
        public string FourthHolder { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
