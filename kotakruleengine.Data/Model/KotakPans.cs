﻿using System;
using System.Collections.Generic;
using System.Text;

namespace kotakruleengine.Data.Model
{
    public class KotakPans
    {
        public long Id { get; set; }
        public long KotakApplicationFileId { get; set; }
        public string Name { get; set;}
        public string PanNumber { get; set; }
    }
}
